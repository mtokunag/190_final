#!/usr/bin/env python

import itertools
from copy import deepcopy
from Queue import PriorityQueue
from read_config import read_config
#from cse_190_assi_3.msg import AStarPath

#path_pub = rospy.Publisher('/result/path_list', AStarPath, queue_size = 10)

# use manhattan distance as heuristic
def manhattan_dist(state, goal):
        return abs(goal[0] - state[0]) + abs(goal[1] - state[1])

# convert state to str so it can be used in dictionary
def to_str(state):
        state_str = ''.join([str(x) for x in state])
        return state_str

def a_star():
    #Read from JSON
    config = read_config()
	
    #Instance Variables
    move_list = config['move_list']
    map_size = config['map_size']
    start = config['start']
    goal = config['goal']
    walls = config['walls']
    pits = config['pits']

    count = 0
    counter = itertools.count()
    frontier = PriorityQueue()
    print(next(counter))
    frontier.put((manhattan_dist(start, goal), next(counter), count, [start]))

    visited = {
            to_str(start) : True
    }

        
    while not frontier.empty():
        _, _, cost, curr_path = frontier.get()
        curr = curr_path[-1]

        visited[to_str(curr)] = True

        # found goal
        if curr == goal:
           # for path in curr_path:
            return curr_path;

        #print(visited)
        # Left
        next_state = [curr[0], curr[1] - 1 if curr[1] > 0 else 0]
        if (not to_str(next_state) in visited) and (not next_state in walls) and (not next_state in pits):
                count = cost + 1
                frontier.put((manhattan_dist(next_state, goal) + count, next(counter), count, deepcopy(curr_path) + [next_state]))

        # Up
        next_state = [curr[0] - 1 if curr[0] > 0 else 0, curr[1]]
        if (not to_str(next_state) in visited) and (not next_state in walls) and (not next_state in pits):
                count = cost + 1
                frontier.put((manhattan_dist(next_state, goal) + count, next(counter), count, deepcopy(curr_path) + [next_state]))

        # Right
        next_state = [curr[0], curr[1] + 1 if curr[1] < map_size[1]-1 else map_size[1]-1]
        if (not to_str(next_state) in visited) and (not next_state in walls) and (not next_state in pits):
                count = cost + 1
                frontier.put((manhattan_dist(next_state, goal) + count, next(counter), count, deepcopy(curr_path) + [next_state]))

        # Down
        next_state = [curr[0] + 1 if curr[0] < map_size[0]-1 else map_size[0]-1, curr[1]]
        if (not to_str(next_state) in visited) and (not next_state in walls) and (not next_state in pits):
                count = cost + 1
                frontier.put((manhattan_dist(next_state, goal) + count, next(counter), count, deepcopy(curr_path) + [next_state]))

        


